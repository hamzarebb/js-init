const { 
    usingArrays,
    objectArray 
} = require('./../index')

describe(`Array manipulation`, () => 
{
    let array
    beforeEach(() => 
    {
        array = usingArrays()
    })

    it(`Should have 8 items`, () => 
    {
        expect(array.length).toBe(8)
    })

    it(`Should have 3 even values`, () =>
    {
        const evenVal = array.filter(value => value % 2 === 0)
        expect(evenVal.length).toBe(3)
    })

    it(`Should have a sum of '87'`, () => 
    {
        const sum = array.reduce((accumulator, value) => accumulator + value, 0)
        expect(sum).toBe(87)
    })

    it(`Should have an odd sum of '43' `, () => 
    {
        const oddSum = array
            .filter(value => value % 2 !== 0)
            .reduce((accumulator, value) => accumulator + value, 0)
        expect(oddSum).toBe(43)
    })

    it(`Should have a global stock of '21'`, () => {
        const globalStock = objectArray
            .map(item => item.stock)
            .reduce((accumulator, value) => accumulator + value, 0)
        expect(globalStock).toBe(21)
    })

    it(`Should be sorted by ascending id`, () => {
        const sortedArray = objectArray
            .sort((a, b) => a.id.localeCompare(b.id))

        expect(sortedArray[2].id).toBe('4ae36')
    })

    it(`Should be sorted by descending name'`, () => {
        const sortedArray = objectArray
            .sort((a, b) => b.name.localeCompare(a.name))

        expect(sortedArray[0].name).toBe('Raviolis')
    })

    it(`Should give an array of product with a stock gt 5`, () => {
        const workArray = [...objectArray]

        for (let i = 0; i < workArray.length; i++ )
        {
            if (workArray[i].stock < 5)
            {
                objectArray.splice(i, 1)
            }
        }

        expect(objectArray.length).toBe(2)
    })
})