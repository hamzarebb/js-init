import { Product } from "../products/product.class"
import { ProductBuilder } from "../products/product-builder.class"
import { NameIsEmptyError } from "../exceptions/name_is_empty.class"
import { IdIsEmptyError } from "../exceptions/id_is_empty.class"
import { StockNotNumeric } from "../exceptions/stock_not_numeric.class"

describe(`Product class`, () => 
{
    it(`Should make an instance of Product`, () => 
    {
        const product = Product.getInstance()
        expect(product).toBeInstanceOf(Product)
    })

    it(`Should give back an id, a name and a stock`, () => 
    {
        const product = Product.getInstance()
        product.id = '1fe24'
        product.name = 'Test'
        product.stock = 10

        expect(product.id).toEqual('1fe24')
        expect(product.name).toEqual('Test')
        expect(product.stock).toEqual(10)
    })

    it(`Should instantiate correctly`, () => {
        const builderInstance = new ProductBuilder()
        expect(builderInstance).toBeInstanceOf(ProductBuilder)
    })

    it(`Should correctly build product`, () => 
    {
        const product = Product.getInstance()
        product.id = '1fe24'
        product.name = 'Test'
        product.stock = 10

        const buildProduct = new ProductBuilder()
        buildProduct.id = '1fe24'
        buildProduct.name = 'Test'
        buildProduct.stock = 10

        expect(buildProduct.id).toEqual(product.id)
        expect(buildProduct.name).toEqual(product.name)
        expect(buildProduct.stock).toEqual(product.stock)
    })

    it(`Should raise an exception for id`, () => 
    {
        const productBuilder = new ProductBuilder()
        productBuilder.name = 'Test'
        productBuilder.stock = 10
      
        expect(() => productBuilder.build()).toThrow(IdIsEmptyError)
        expect(() => productBuilder.build()).toThrow("Id shouldn't be empty")
    })
      
    it(`Should raise an exception for name`, () => 
    {
        const productBuilder = new ProductBuilder()
        productBuilder.id = 'Test'
        productBuilder.stock = 10
      
        expect(() => productBuilder.build()).toThrow(NameIsEmptyError)
        expect(() => productBuilder.build()).toThrow("Name shouldn't be empty")
    })
      
    it(`Should raise an exception for stock`, () => 
    {
        const productBuilder = new ProductBuilder()
        productBuilder.id = 'Test'
        productBuilder.name = 'Test'
        productBuilder.stock = 'Test'
      
        expect(() => productBuilder.build()).toThrow(StockNotNumeric)
        expect(() => productBuilder.build()).toThrow('Stock should be a number')
    })
})