const { 
    primitiveAffectation, 
    birthDate, 
    myBirthDate,
    objectCopy,
    usingPrototypePattern
} = require('./../index')

describe(`index.js`, () => 
{
    it(`Should return 'Rebbani'`, () => 
    {
        expect(primitiveAffectation()).toBe('Rebbani')
    })

    it(`Should have the same reference`, () => 
    {
        expect(birthDate).toStrictEqual(myBirthDate)
    })

    it(`Should have differ reference`, () => 
    {
        const theDate = new Date(1998, 4, 12)

        const otherDate = objectCopy(theDate)
        expect(theDate === otherDate).toBeFalsy()

        const protoDate = usingPrototypePattern(theDate)
        expect(theDate === protoDate).toBeFalsy()
    })

    it(`Should throw an error`, () => 
    {
        expect(() => usingPrototypePattern('toto')).toThrow(TypeError)
    })
})
