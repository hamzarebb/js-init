const primitiveAffectation = () => 
{
    let lastname = 'Rebbani'
    return lastname
}

const generateBirthDate = () => new Date()
const myBirthDate = generateBirthDate()
const birthDate = myBirthDate

const objectCopy = (aDate) =>
{
    return {
        ...aDate
    }
}

const usingPrototypePattern = (aDate) =>
{
    if (!(aDate instanceof Date))
    {
        throw new TypeError('aDate is not a valid data')
    }

    const anonymousDate = new Date()
    anonymousDate.setDate(aDate.getDate())
    anonymousDate.setMonth(aDate.getMonth())
    anonymousDate.setFullYear(aDate.getFullYear())

    return anonymousDate
}

const usingArrays = () => 
{
    const myArray = [
        1,
        2,
        3,
        5,
        8,
        13,
        21,
        34
    ]
    myArray.push(55)
    myArray.pop()

    return myArray
}

const objectArray = 
[
    {
        "id": "1fe34",
        "name": "Bananes",
        "stock": 12
    },
    {
        "id": "4ae36",
        "name": "Café en grain",
        "stock": 6
    },
    {
        "id": "3cc52",
        "name": "Raviolis",
        "stock": 3
    }
]

module.exports = 
{ 
    primitiveAffectation, 
    birthDate, 
    myBirthDate,
    objectCopy,
    usingPrototypePattern,
    usingArrays,
    objectArray 
}