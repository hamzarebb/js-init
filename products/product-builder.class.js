import { Builder } from "../_helpers/builder.class"
import { Product } from "./product.class"
import { StockNotNumeric } from "../exceptions/stock_not_numeric.class"
import { NameIsEmptyError } from "../exceptions/name_is_empty.class"
import { IdIsEmptyError } from "../exceptions/id_is_empty.class"

export class ProductBuilder extends Builder
{
    /**
     * ID attribute for new product
     * @var string
     */
    id = ''

    /**
    * Name of the new product
    * @var name
    */
    name = ''

    /**
    * Stock of product
    * @var number
    */
    stock = 0

    /**
     * Build a concrete product
     * Throws exceptions
     * @return Product
     * @see Builder
     * @override
     */
    build() {
        if (this.id === '') throw new IdIsEmptyError()
    
        if (this.name === '') throw new NameIsEmptyError()
    
        if (isNaN(this.stock) || this.stock < 0) throw new StockNotNumeric()
    
        const product = Product.getInstance()
        product.id = this.id
        product.name = this.name
        product.stock = this.stock
    
        return product
      }
}