import {
    StockNullError
} from "../exceptions/stock_not_null.class"
import NameIsEmptyError from '../exceptions/name_is_empty.class'
import { IdIsEmptyError } from "../exceptions/id_is_empty.class"
import StockNotNumeric from "../exceptions/stock_not_numeric.class"

export class Product
{
/**
* ID of the product
* @var string
*/
    #id = ''

/**
* Name of the product
* @var name
*/
    #name = ''

/**
 * Stock of product
 * @var number
 */
    #stock = 0

    constructor(doIt = null) 
    {
        if (!doIt)
            throw new Error(`Cannot instanciate directly a new Product`)
    }

    get id() { return this.#id }
    set id(id) {
        if (!id)
            return new IdIsEmptyError() 
        this.#id=id 
    }

    get name() { return this.#name }
    set name(name) 
    { 
        if (!name)
            return NameIsEmptyError()
        this.#name=name
    }

    get stock() { return this.#stock }
    set stock(stock)
    {
        if (isNaN(stock)) 
            return new StockNotNumeric()

        if (stock === null || stock === undefined)
            return new StockNullError()

        if (stock < 0)
            return StockNegativeError()

        this.#stock=stock
    }

    static getInstance()
    {
        return new Product(true)
    }
}