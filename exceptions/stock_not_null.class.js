export class StockNullError extends Error
{
    constructor()
    {
        super("Stock shouldn't be empty")
        this.status = 400
        this.message = "Stock shouldn't be empty"
    }
}