class StockNegativeError extends Error 
{
    constructor() 
    {
        super("Stock should be a positive integer")
        this.status = 400
        this.message = "Stock should be a positive integer"
    }
}
