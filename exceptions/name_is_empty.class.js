export class NameIsEmptyError extends Error 
{
    constructor() 
    {
        super("Name shouldn't be empty")
        this.status = 400
        this.message = "Name shouldn't be empty"
    }
}