export class IdIsEmptyError extends Error 
{
    constructor() 
    {
        super("Id shouldn't be empty")
        this.status = 400
        this.message = "Id shouldn't be empty"
    }
}