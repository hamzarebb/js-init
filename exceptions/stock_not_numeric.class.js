export class StockNotNumeric extends Error
{
    constructor()
    {
        super("Stock should be a number")
        this.status = 400
        this.message = "Stock should be a number"
    }
}